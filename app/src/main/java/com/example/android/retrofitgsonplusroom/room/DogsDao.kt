package com.example.android.retrofitgsonplusroom.room

import androidx.room.*
import com.example.android.retrofitgsonplusroom.model.Dogs
import io.reactivex.Flowable

@Dao
interface DogsDao {
    @Query("SELECT * FROM Dogs")
    fun getAll(): Flowable<List<Dogs>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCurrent(imageurl: Dogs)
}