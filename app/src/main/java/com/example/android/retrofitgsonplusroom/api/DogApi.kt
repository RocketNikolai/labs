package com.example.android.retrofitgsonplusroom.api

import com.example.android.retrofitgsonplusroom.model.Dogs
import io.reactivex.Observable
import retrofit2.http.GET

public interface DogApi {
    @GET("images")
    fun getDogs(): Observable<Dogs>
}