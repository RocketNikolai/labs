package com.example.android.retrofitgsonplusroom.room

import android.content.Context
import androidx.room.*
import com.example.android.retrofitgsonplusroom.model.Dogs

@Database(entities = arrayOf(Dogs::class), version = 1)
@TypeConverters(Converters::class)
abstract class DogsImageDatabase : RoomDatabase() {
    abstract fun dogsDao(): DogsDao

    companion object {

        @Volatile private var INSTANCE: DogsImageDatabase? = null

        fun getInstance(context: Context): DogsImageDatabase =
            INSTANCE ?: synchronized(this) {
                INSTANCE ?: buildDatabase(context).also { INSTANCE = it }
            }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(context.applicationContext,
                DogsImageDatabase::class.java,"dogsimagedatabase" )
                .build()
    }
}