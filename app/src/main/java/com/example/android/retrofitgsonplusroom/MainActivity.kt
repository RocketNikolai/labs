package com.example.android.retrofitgsonplusroom

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.android.retrofitgsonplusroom.adapters.MyAdapter
import com.example.android.retrofitgsonplusroom.api.DogApi
import com.example.android.retrofitgsonplusroom.model.Dogs
import com.example.android.retrofitgsonplusroom.room.DogsImageDatabase
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.internal.operators.parallel.ParallelDoOnNextTry
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory


class MainActivity : AppCompatActivity() {

    lateinit var dogslist: Dogs
    lateinit var context: Context
    lateinit var mydb: DogsImageDatabase
    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: MyAdapter
    private lateinit var viewManager: RecyclerView.LayoutManager
    private lateinit var compositeDisposable: CompositeDisposable
    var counter = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        context = applicationContext
        mydb = DogsImageDatabase.getInstance(context)

        /*
        When you click on next or previous buttons, you store to
        Room the same dogslist(s).
        When you click on room button, you can see data
        in db on Logs(log.i tag getAll).
        Rotate states are not supported.
        * */

        //Next Button
        button_next.setOnClickListener {
            counter++
            if (counter >= dogslist.message.size - 1) {
                counter = 0
            }
            Toast.makeText(applicationContext, "Next", Toast.LENGTH_LONG).show()
            Single.fromCallable {mydb.dogsDao().insertCurrent(dogslist)}.subscribeOn(Schedulers.io())
                .subscribe({
                    Log.i("insertCurrent", "+++");
                }, {
                    Log.i("Error ", "---");
                })
        }

        //Previous Button
        button_previous.setOnClickListener {
            Toast.makeText(applicationContext, "Previous", Toast.LENGTH_LONG).show()
            counter--
            if (counter <= 0) {
                counter = dogslist.message.size - 1
            }
            Single.fromCallable {mydb.dogsDao().insertCurrent(dogslist)}.subscribeOn(Schedulers.io())
                .subscribe({
                    Log.i("insertCurrent ", "+++");
                }, {
                    Log.i("Error ", "---");
                })
        }

        //Room Button
        button_room.setOnClickListener {
            Toast.makeText(applicationContext, "Room", Toast.LENGTH_LONG).show()

            mydb.dogsDao().getAll()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    Log.d("getAll", "dogs image urls" + it[0].message)
                },{
                    Log.d("Error:getAll", "lols")
                })
        }

        //Check ImageView


        viewManager = LinearLayoutManager(applicationContext)
        viewAdapter = MyAdapter()
        recyclerView = findViewById<RecyclerView>(R.id.imageView_dog).apply {
            // use a linear layout manager
            layoutManager = viewManager
            // specify an viewAdapter (see also next example)
            adapter = viewAdapter
        }

        //Retrofit
        val retrofit = Retrofit.Builder()
            .baseUrl("https://dog.ceo/api/breed/hound/")
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
        val service = retrofit.create(DogApi::class.java)

        compositeDisposable.add(service.getDogs()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    dogslist = it
                    viewAdapter.setDog(it)
                    Log.i("StringMessage", it.message.toString())
                }, {
                    Log.i("Subscribe ", "Error")
                }))
    }

    override fun onStop() {
        super.onStop()
        compositeDisposable.clear()
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.clear()
    }
}
