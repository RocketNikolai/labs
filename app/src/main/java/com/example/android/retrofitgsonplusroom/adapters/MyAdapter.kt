package com.example.android.retrofitgsonplusroom.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.android.retrofitgsonplusroom.R
import com.example.android.retrofitgsonplusroom.model.Dogs



var counter: Int = 1
class MyAdapter() : RecyclerView.Adapter<MyAdapter.MyViewHolder>() {
    var list: Dogs? = null
    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder.
    // Each data item is just a string in this case that is shown in a TextView.
    class MyViewHolder(view: View) : RecyclerView.ViewHolder(view){
        var number: TextView
        var imagedog: ImageView
        init {
            number = view.findViewById(R.id.number)
            imagedog = view.findViewById(R.id.imageview)
        }
    }



    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): MyViewHolder {

        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_list, parent, false) as LinearLayout
        return MyViewHolder(view)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        val positions = list!!.message[position]
        holder.number.text = counter.toString()
        Glide.with(holder.imagedog.context)
            .load(positions)
            .into(holder.imagedog)
        counter++
    }

    // Return the size of your dataset (invoked by the layout manager)

    override fun getItemCount(): Int {
        val size = list?.message?.size ?: 0
        return size
    }

    fun setDog(dogs: Dogs) {
        this.list = dogs
        notifyDataSetChanged()
    }
}