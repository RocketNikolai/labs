package com.example.android.retrofitgsonplusroom.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

@Entity
data class Dogs(
        @PrimaryKey
        var imageurlid: Int,

        @SerializedName("status")
        @Expose
        val status: String,

        @ColumnInfo(name = "imageurl")
        @SerializedName("message")
        @Expose
        val message: List<String>)